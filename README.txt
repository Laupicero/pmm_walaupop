El objetivo del proyecto es que aprovechéis el tiempo en vacaciones repasando conceptos de Angular e INVESTIGANDO diferentes librerías y posibilidades que normalmente ofrecen las aplicaciones móviles. 

Se trata de simular un "mini-wallapop": tras ingresar en una página de login, se mostrarán los productos actualmente en venta. 
Cuando seleccionamos uno de ellos vemos sus detalles, su localización en un mapa y podremos comprarlo. 

-------------------------------------------------------------------------------------------------------------------------------------
Requisitos y sugerencias:

- Tras loguearnos, veremos la lista de productos. 
Podéis investigar los "guards" en Angular para evitar que se navegue a páginas no autorizadas.

- Los datos de los productos en venta estarán en forma de archivo JSON en el servidor
(hasta enero no empezamos con la parte de back-end),

- Para que al seleccionar cada producto se nos lleve a la página de detalles y compra usaremos enrutado.
Podéis investigar las "rutas con parámetros" en el libro para evitar tener que crear un nuevo componente para cada producto.

- Podéis investigar cómo usar la API de Google Maps y cómo integrar los mapas en vuestra aplicación. 

- También podéis investigar cómo integrar Paypal en JavaScript. 

- Para la parte visual de la alicación podéis investigar los componentes Material 
que ofrece el propio equipo de Angular como alternativa a BootStrap.

-------------------------------------------------------------------------------------------------------------------------------------